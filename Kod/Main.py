import cv2 as cv
import os
import numpy as np
import ctypes
import time
import datetime
import sys

def genLogger():
    import logging
    LOG_FILENAME = os.path.join("project.log")
    FORMAT = "%(asctime)s : %(message)s"
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    if logger.handlers:
        logger.handlers = []
    fh = logging.FileHandler(LOG_FILENAME)
    formatter = logging.Formatter(FORMAT, datefmt='%Y-%d-%m %H:%M:%S')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return logger

def CheckFolder(path):
    dir = os.path.dirname(path)
    if not os.path.exists(dir):
        os.makedirs(dir)

#   MAIN   #

logger = genLogger()

logger.info('Main program initiated.')
        
FaceRecognizer = cv.face.LBPHFaceRecognizer_create()
CheckFolder("Trainer/")
FaceRecognizer.read("Trainer/trainer.yml")
FaceDetector = cv.CascadeClassifier("haarcascade_frontalface_default.xml")

videoCamera = cv.VideoCapture(0)

countRight = 0
countWrong = 0

timeNow = datetime.datetime.now()
timeNow = timeNow.second

while(True):
    
    timeNowAux = datetime.datetime.now()
    timeNowAux = timeNowAux.second
    if(timeNowAux > timeNow + 8):
        logger.info('No face detected for 5 seconds. Locking workstation.')
        videoCamera.release()
        cv.destroyAllWindows()
        ctypes.windll.user32.LockWorkStation()
        sys.exit()
    
    ret, image = videoCamera.read()
    imageGray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    
    faces = FaceDetector.detectMultiScale(imageGray, 1.3, 5)
    
    for(x, y, width, height) in faces:
        
        cv.rectangle(image, (x-20, y-20), (x+width+20, y+height+20), (0,0,0), 4)
        Id, confidence = FaceRecognizer.predict(imageGray[y:y+height, x:x+width])
        
        if(confidence>80):
            print("Intruder! " + str(countWrong))
            cv.rectangle(image, (x-20, y-20), (x+width+20, y+height+20), (0,0,255), 4)
            cv.putText(image, "Intruder!", (x,y-40), cv.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,0,255), 2)
            countWrong += 1
        else:
            print("Verified user. " + str(countRight))
            cv.rectangle(image, (x-20, y-20), (x+width+20, y+height+20), (0,255,0), 4)
            cv.putText(image, "Verified user. ID: " + str(Id), (x,y-40), cv.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,255,0), 2)
            countRight += 1
        
        # LOCK IF INTRUDER #
        if(countWrong == 5):
            logger.info('Intruder detected. Locking workstation.')
            print("LOCKING WORKSTATION!")
            videoCamera.release()
            cv.destroyAllWindows()
            ctypes.windll.user32.LockWorkStation()
            sys.exit()
        if(countRight == 10):
            logger.info('Verified user confirmed. User ID: %s', str(Id))
            print("Welcome!")
            videoCamera.release()
            cv.destroyAllWindows()
            sys.exit()
    
    cv.imshow("Webcam", image)
    
    if cv.waitKey(100) & 0xFF == 27:
        break
    
videoCamera.release()
cv.destroyAllWindows()