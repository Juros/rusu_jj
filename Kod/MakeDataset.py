import cv2 as cv
import os

def genLogger():
    import logging
    LOG_FILENAME = os.path.join("project.log")
    FORMAT = "%(asctime)s : %(message)s"
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    if logger.handlers:
        logger.handlers = []
    fh = logging.FileHandler(LOG_FILENAME)
    formatter = logging.Formatter(FORMAT, datefmt='%Y-%d-%m %H:%M:%S')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return logger

def createDatasetFolder(path):
    dir = os.path.dirname(path)
    if not os.path.exists(dir):
        os.makedirs(dir)

#   MAIN   # 

logger = genLogger()

logger.info('Make Dataset program initiated.')        

videoCamera = cv.VideoCapture(0)

FaceDetector = cv.CascadeClassifier('haarcascade_frontalface_default.xml')

idFace = 1
counter = 0

createDatasetFolder("Dataset/")

while(True):
    _,frameImage = videoCamera.read() 
    frameGray = cv.cvtColor(frameImage, cv.COLOR_BGR2GRAY)
    
    listFaces = FaceDetector.detectMultiScale(frameGray, 1.4, 5)
    
    for(x, y, width, height) in listFaces:
        cv.rectangle(frameImage, (x,y), (x+width, y+height), (255,0,0), 2)
        counter += 1
        
        cv.imwrite("Dataset/User_" + str(idFace) + '_' + str(counter) + ".jpg", frameGray[y:y+height,x:x+width])
        cv.imshow("Creating Dataset.", frameImage)
        
    if cv.waitKey(100) & 0xFF == 27:
        logger.info('Make Dataset program finished prematurely.') 
        break
    elif counter > 300:
        logger.info('Make Dataset program finished successfully. 300 images saved.') 
        break

videoCamera.release()
cv.destroyAllWindows()