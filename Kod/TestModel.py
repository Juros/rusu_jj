import cv2 as cv
import os
import numpy as np
from PIL import Image
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt

def ImportImages(path):
    imagePaths = [os.path.join(path,f) for f in os.listdir(path)]
    faceSamples = []
    idFace = []
    
    for imagePath in imagePaths:
        PIL_image = Image.open(imagePath).convert('L')
        numpyImage = np.array(PIL_image, 'uint8')

        idImage = int(os.path.split(imagePath)[-1].split("_")[1])
        
        faces = FaceDetector.detectMultiScale(numpyImage)
        
        for(x, y, width, height) in faces:
            faceSamples.append(numpyImage[y:y+height, x:x+width])
            idFace.append(idImage)
    
    return faceSamples, idFace

def CheckFolder(path):
    dir = os.path.dirname(path)
    if not os.path.exists(dir):
        os.makedirs(dir)

def plot_confusion_matrix(c_matrix):
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x), 
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)

    fig.colorbar(res)
    plt.show()

FaceRecognizer = cv.face.LBPHFaceRecognizer_create()
CheckFolder("Trainer/")
FaceRecognizer.read("Trainer/trainer.yml")
FaceDetector = cv.CascadeClassifier("haarcascade_frontalface_default.xml")

# OD 150 SLIKA, 50 je ovlašteni krosnik, dok je 100 neovlašteni korisnik
Faces, faceID = ImportImages("TestImages")
# if faceID = 0 onda user, ako je 1 onda uljez....

outputActual = faceID
outputPredict = []

for face in Faces:
    aux = FaceRecognizer.predict(face)
    if(aux[1] < 80):
        outputPredict.append(0)
    else:
        outputPredict.append(1)
    aux = 0

confMat = confusion_matrix(outputActual, outputPredict)
print(confMat)
plot_confusion_matrix(confMat)