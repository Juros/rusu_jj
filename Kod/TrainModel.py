import cv2 as cv
import os
import numpy as np
from PIL import Image

def genLogger():
    import logging
    LOG_FILENAME = os.path.join("project.log")
    FORMAT = "%(asctime)s : %(message)s"
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    if logger.handlers:
        logger.handlers = []
    fh = logging.FileHandler(LOG_FILENAME)
    formatter = logging.Formatter(FORMAT, datefmt='%Y-%d-%m %H:%M:%S')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return logger

def CreateTrainingFolder(path):
    dir = os.path.dirname(path)
    if not os.path.exists(dir):
        os.makedirs(dir)

def ImportImages(path):
    imagePaths = [os.path.join(path,f) for f in os.listdir(path)]
    faceSamples = []
    idFace = []
    
    for imagePath in imagePaths:
        PIL_image = Image.open(imagePath).convert('L')
        numpyImage = np.array(PIL_image, 'uint8')

        idImage = int(os.path.split(imagePath)[-1].split("_")[1])
        
        faces = FaceDetector.detectMultiScale(numpyImage)
        
        for(x, y, width, height) in faces:
            faceSamples.append(numpyImage[y:y+height, x:x+width])
            idFace.append(idImage)
    
    return faceSamples, idFace

#   MAIN   #
    
logger = genLogger()

logger.info('Training program initiated.')  

FaceRecognizer = cv.face.LBPHFaceRecognizer_create()
FaceDetector = cv.CascadeClassifier("haarcascade_frontalface_default.xml")

Faces, idFaces = ImportImages("Dataset")

FaceRecognizer.train(Faces, np.array(idFaces))

CreateTrainingFolder("Trainer/")
FaceRecognizer.write("Trainer/trainer.yml")
logger.info('Model trained successfully.')  